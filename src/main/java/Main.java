package Homework6;

public class Main {
    public static void main(String[] args) {
        String[] habits = {"run", "eat", "sleep"};
        String [][] schedule = {new String[]{Homework6.DayOfWeek.Sunday.name(), "Code"},{Homework6.DayOfWeek.Monday.name(),"Code"}};

        Homework6.Human mother = new Homework6.Human("Katerina", "Snowden", 1968, 100, schedule);
        Homework6.Human father = new Homework6.Human("Edward", "Snowden", 1956, 100, schedule);
        Homework6.Pet pet = new Homework6.Pet(Homework6.Species.Dog,"Alabash",3,100, habits);
        Homework6.Human child = new Homework6.Human("Steve","Snowden",1993,100,schedule);

        Homework6.Family family = new Homework6.Family(mother, father, 1);
        family.addChild(child);
        System.out.println(family.toString() + "\n");
        System.out.println(pet.toString() + "\n");
        System.out.println(child.toString());
    }
}